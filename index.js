const express = require('express')
const bodyParser = require('body-parser')
const flash = require('connect-flash')//1
const session = require('express-session')//4
const userRoute = require('./controller/user.route')
const cookieParser= require('cookie-parser')
const homeRoute = require('./controller/home.route')
require('./lib/dbconnect') // không dùng mà chỉ để start DB nên ghi không có const
const app = express()


app.set('view engine','ejs')
app.use(bodyParser.urlencoded({extended:false}))
app.use(cookieParser())
//5
app.use(session({
    secret:'secret',
    resave:true,
    saveUninitialized:true
}))
app.use(flash())//2
app.use((req,res,next)=>{//3
    res.locals.error_message = req.flash('error_message') //middlewave
    next()
})

app.use('/user',userRoute)
//app.use('/',homeRoute)

app.get('/',(req,res)=>{
    const token = req.cookies.token
    res.send({token})
})
app.use('/',homeRoute)

app.listen(3000,console.log('Server is running!'))
