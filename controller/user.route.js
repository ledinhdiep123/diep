const express = require('express')
const {hash,compare} = require('../lib/bcrypt')
const Usermodel = require('../model/User')
const route = express.Router()
const {verify,sign} = require('../lib/jwt')

route.get('/register',(req,res)=>{
    res.render('register')
})
route.post('/register',(req,res)=>{
    const{email,password,password_comfirmation}=req.body
    if(password!=password_comfirmation){
        req.flash('error_message','Confirmation password not match')
        res.redirect('/user/register')
    }
    if(!email || email==''){
        req.flash('error_message','Please enter email')
        return res.redirect('/user/register')
    }
    hash(password)
    .then(hash=>{//cho hash
        return Usermodel.create({
            email:email,
            password: hash
        })
    }).then(()=>{
        return res.redirect('/user/login')
    })//cho create
    .catch(()=>{
        req.flash('error_message','cannot register account')
        return res.redirect('/user/register')
    })
})
route.get('/login',(req,res)=>{
    res.render('login')
})

route.post('/login',(req,res)=>{
    const {email} = req.body
    Usermodel.findOne({email})
    .then(user=>{
        if(!user){
            req.flash('error_message','Cannot find user')
            return res.redirect('/user/login')
        }else{
            // compare(password,user.password)
            // .then(check=>{
            //     if(check) //save into cookie, khong ghi pass vao jwt
            //         return sign({id:user._id,email:user.email})
            //     else{
            //         req.flash('error_message','Cannot find user')
            //         return res.redirect('/user/login')
            //     }
            // })
            return res.send({user: req.body.email})
            // .then(token=>{
            //     res.cookie('token',token,{maxAge:3600000}).redirect('/')
            //     //res.send({token})
            // })
            // .catch(err=>{
            //     req.flash('error_message','Something Wrong!')
            //     return res.redirect('/user/login')
            // })
        }
    })
    .catch(err=>{
        req.flash('error_message','Something Wrong!')
        return res.redirect('/user/login')
    })
}) 

module.exports = route